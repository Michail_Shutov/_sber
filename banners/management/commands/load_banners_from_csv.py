import os

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from banners.models import Banner, BannerCategory


class Command(BaseCommand):
    help = 'Load banners to Data Base (current will be deleted)'

    def add_arguments(self, parser):
        parser.add_argument(
            '--file',
            dest='path_to_file',
            type=str,
            help='Path to csv-file')

    @transaction.atomic
    def handle(self, *args, path_to_file: str = None, **options):

        if not (os.path.exists(path_to_file) and os.path.isfile(path_to_file)):
            raise CommandError('Invalid file path')

        # убираем старые
        BannerCategory.objects.all().delete()
        Banner.objects.all().delete()

        # кэшируем в словаре
        cached_categories = {}

        with open(path_to_file) as source:
            for banner_data in source:
                url, count, *categories = banner_data.strip().split(';')
                banner = Banner.objects.create(url=url, count=count)
                banner_categories = []
                new_categories = []
                for category_name in categories:
                    cached_category = cached_categories.get(category_name)
                    if cached_category:
                        banner_categories.append(cached_category)
                    else:
                        new_categories.append(BannerCategory(name=category_name))
                if new_categories:
                    created = BannerCategory.objects.bulk_create(new_categories)
                    banner_categories.extend(created)
                    cached_categories.update({c.name: c for c in created})
                banner.categories.add(*banner_categories)
