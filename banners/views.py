import numpy

from django.core.cache import cache
from django.db.models import F
from django.shortcuts import render
from django.views import View

from .models import Banner


class BannerView(View):
    http_method_names = ('get',)

    LAST_BANNER_KEY = 'banners:last_banner'
    # указывает, сколько баннеров извлечь из БД для сравнения
    # количеству оплаченных просмотров)
    # частоты показа (для пропорциональности частоты показа количеству
    QS_BANNERS_COUNT = 10

    def get(self, request):
        """
        отдаёт баннер удавлетворяющий фильтру по категории (по дуфолту - любая)

        пример запроса:
        GET: /banners/?category=c4&category=c1
        """
        render_dict = {}
        banners = Banner.objects.filter(count__gt=0).order_by('?')

        categories = request.GET.getlist('category')
        if categories:
            banners = banners.filter(categories__name__in=categories).distinct()

        if banners:
            # получаем рандомный баннер (удовлетворяющий условиям запроса)
            banners_list = None
            last_banner = cache.get(self.LAST_BANNER_KEY)
            if last_banner:
                banners_list = banners.exclude(id=last_banner)[:self.QS_BANNERS_COUNT]
            if not banners_list:  # не проверяем exists'ом так как так больше запросов
                banners_list = banners[:self.QS_BANNERS_COUNT]

            priorities = [b.count for b in banners_list]
            # теперь нам надо привести приоритеты, чтобы их сумма была равна единие
            sum_p = sum(priorities)
            priorities_p = [tmp / sum(priorities) for tmp in priorities]
            priorities_p[0] += 1 - sum(priorities_p)  # так как работаем с float докидываем для точности

            banner = numpy.random.choice(banners_list, p=priorities_p)

            # уменьшаем просмотры
            Banner.objects.filter(id=banner.id).update(count=F('count')-1)
            # запоминаем, что показали этот баннер последним
            cache.set(self.LAST_BANNER_KEY, banner.id, 60*5)

            render_dict['banner'] = banner

        # как я понял, если баннера нет отдаётся пустой html
        return render(request, 'banners/banner.html', render_dict)
