from django.core import management
from django.test import TestCase
from django.urls import reverse

from banners.models import Banner, BannerCategory
from urllib.parse import urlencode


class ProjectTests(TestCase):
    """
    Проверяет работоспособность действий с проектами
    """

    def test_load_from_csv(self):
        # тест получения баннеров из csv
        management.call_command('load_banners_from_csv', file='apps/banners/banners_test_data.csv')
        print(Banner.objects.count(), BannerCategory.objects.count())
        self.assertEqual(Banner.objects.count(), 10)
        self.assertEqual(BannerCategory.objects.count(), 4)

    def test_counter(self):
        management.call_command('load_banners_from_csv', file='apps/banners/banners_test_data.csv')
        url = '%s?%s' % (reverse('banner'), urlencode({'category': 'c4'}))

        response = self.client.get(url)
        self.assertIn('banner', response.context)
        response = self.client.get(url)
        self.assertNotIn('banner', response.context)
