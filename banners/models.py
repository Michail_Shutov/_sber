from django.db import models


class BannerCategory(models.Model):
    name = models.CharField(max_length=255)


class Banner(models.Model):
    url = models.URLField()
    count = models.IntegerField()
    categories = models.ManyToManyField(BannerCategory)
